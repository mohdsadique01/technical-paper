# OSI MODEL


## Abstract:

- Due to the urgency in the need for standards for heterogeneous computer networks,
International Standard Organization (ISO) created a new subcommittee for ―Open System Interconnection‖
(ISO/TC97/SC16) in 1977. The first priority of subcommittee 16 was to develop architecture for Open System
Interconnection which could serve as a frame work for the definition of standard protocols. As a result 18 months of
studies and discussions, SC16 adopted a layered architecture comprising seven layers (Physical, Data Link,
Network, Transport, Session, Presentation, and Application). In July 1979 the specifications of this architecture,
established by SC16, were passed under the name of ―OSI Reference Model‖ to Technical committee 97
―Data processing‖ along with recommendations to start officially, on this basis a set of protocols standardizations
to start projects to cover the most urgent needs. These recommendations were adopted by TC97 at the end of
1979 as the basis for the following development of standards for Open System Interconnection within ISO.
This paper explains the OSI Reference Model, which comprises of seven different layers. Each layer having their
own responsibilities.

## Table of Content
**1**.) **Introduction**

**2**.) **Methodology**

**3**.) **Physical layer**

**4**.) **DATA-LINK LAYER**

**5**.) **NETWORK LAYER**

**6**.) **TRANSPORT LAYER**

**7**.) **SESSION LAYER**

**8**.) **PRESENTATION LAYER**

**9**.) **APPLICATION LAYER**

**10**.) **The advantages of the OSI model**

**11**.) **The dis-advantages of the OSI model**

**12**.) **Conclusion**

**13**.) **Reference**




## INTRODUCTION:

  Networking evolved from the basic principle of moving data from one computer to another. The first method involved copying data to a storage media such as a floppy disk and then taking that storage media to another computer and copying the data. This was charmingly referred to as sneaker-net. As more efficient means were discovered—namely, electricity on a copper wire—networking became more popular. However, there were no standards in place. This meant that one network manufacturer implemented a different means of data transfer than another. If you had an IBM network, you purchased only IBM network devices. In 1984, a group known as theInternational Organization for Standardization (ISO) created a model called the Open Systems Interconnect (OSI).This model defined guidelines for interoperability between network manufacturers. A company could now mix and match network devices and protocols from various manufacturers in its own network without being locked into using a single vendor. It also had a great side effect: Competition meant lower prices. Although the OSI model defined a set of standards, it is important to note that it is merely a model. Many other models exist in the networking industry; however, understanding a single model gives us the capability of understanding other models in the future. The OSI model is the most widely taught as the foundation for this knowledge. By using a layered model, we can categorize the procedures that are necessary to transmit data across a network. Let’s explore this in more detail. Imagine that we are developers and we are about to create a new protocol for communication across a network. First, we need to define the term protocol: A protocol is a set of guidelines or rules of communication. Some think of a protocol as a dialect of a language; this is erroneous. The British and the Americans both speak the same language: English. However, certain words differ in meaning between the two countries. The timing of the exchange of words between the two cultures can also lead to difficulties in complete understanding. A protocol, then, is more than just the words of computers. It also includes the timing and the same dictionary so that at any time, both computers using the same protocol have an exact, complete understanding of each other. Each layer has specific functions it is responsible for• All layers work together in the correct order to move data around a network.






- ![photo](https://miro.medium.com/max/1024/1*17Zz6v0HWIzgiOzQYmO6lA.jpeg)

**Image of division of layer** 


- ![osi](https://www.guru99.com/images/1/092119_0729_LayersofOSI1.png) 
 

## METHODOLOGY USED
According to OSI document [ISO7498], the purpose of OSI is as follows:– "The purpose of this International
Standard Reference Model of Open Systems Interconnection is to provide a common basis for the coordination of
standards development for the purpose of system inter-connection, while allowing existing standards to be placed
into perspective within the overall Reference Model

#### OSI LAYER QUICK SUMMARY
- ![photo](https://media.geeksforgeeks.org/wp-content/uploads/20220511230638/OSImodelakhilabhilash01.png)


## PHYSICAL LAYER
- In the seven-layer OSI model of computer networking, the physical layer or layer 1 is the first and lowest layer. The implementation of this layer is often termed PHY.

- The physical layer consists of the basic networking hardware transmission technologies of a network. It is a fundamental layer underlying the logical data structures of the higher level functions in a network. Due to the plethora of available hardware technologies with widely varying characteristics, this is perhaps the most complex layer in the OSI architecture.

- The physical layer defines the means of transmitting raw bits rather than logical data packets over a physical link connecting network nodes. The bit stream may be grouped into code words or symbols and converted to a physical signal that is transmitted over a hardware transmission medium. The physical layer provides an electrical, mechanical, and procedural interface to the transmission medium. The shapes and properties of the electrical connectors, the frequencies to broadcast on, the modulation scheme to use and similar low-level parameters, are specified here.

- Within the semantics of the OSI network architecture, the physical layer translates logical communications requests from the data link layer into hardware-specific operations to affect transmission or reception of electronic signals.


- ![PHOTO](https://static.studytonight.com/computer-networks/images/Figure28-1.png)

## DATA-LINK LAYER

- This is the second layer of OSI model. The data link layer is responsible for getting the data packaged from the
physical layer. The data link layer is often subdivided into two parts Logical Link Control (LLC) and Medium
Access Control (MAC).The main function of this layer are handles the physical transfer, farming ( the assembly of
data into a single unit or block), flow control and error-control Functions over a single transmission link. The data
link layer provides the network layer

- ![PHOTO](https://www.cspsprotocol.com/wp-content/uploads/2020/04/Data-Link-Layer-In-OSI-Model.png)

## NETWORK LAYER

- The network Layer controls the operation of the subnet. The main aim of this layer is to deliver packets from source to destination across multiple links (networks). If two computers (system) are connected on the same link, then there is no need for a network layer. It routes the signal through different channels to the other end and acts as a network controller.

- It also divides the outgoing messages into packets and to assemble incoming packets into messages for higher levels.

- In broadcast networks, the routing problem is simple, so the network layer is often thin or even non-existent.

- ![PHOTO](https://kullabs.com/uploads/Network_Layer1.jpg)

## TRANSPORT LAYER

- In computer networking, the transport layer is a conceptual division of methods in the layered architecture of protocols in the network stack in the Internet Protocol Suite and the Open Systems Interconnection (OSI). The protocols of the layer provide host-to-host communication services for applications.[1] It provides services such as connection-oriented data stream support, reliability, flow control, and multiplexing.

- The details of implementation and semantics of the Transport Layer of the TCP/IP model (RFC 1122), which is the foundation of the Internet, and the Open Systems Interconnection (OSI) model of general networking, are different. In the OSI model the transport layer is most often referred to as Layer 4 or L4, while numbered layers are not used in TCP/IP.

- The best-known transport protocol of TCP/IP is the Transmission Control Protocol (TCP), and lent its name to the title of the entire suite. It is used for connection-oriented transmissions, whereas the connectionless User Datagram Protocol (UDP) is used for simpler messaging transmissions. TCP is the more complex protocol, due to its stateful design incorporating reliable transmission and data stream services. Other prominent protocols in this group are the Datagram Congestion Control Protocol (DCCP) and the Stream Control Transmission Protocol (SCTP).
- ![PHOTO](https://kullabs.com/uploads/Transport_Layer1.jpg)

## SESSION LAYER

- This is the fifth layer of OSI. It sets up and clear communication channels between two communicating component.
The session layer decides when to turn communication on and off between two computers- it provides the
mechanisms that control the data exchange process and coordinates the interaction between them. It provides
coordination of the communication in an ordering manner. It determines one way and two way communications and
manage the dialogue between both parties. 

- ![PHOTO](https://static.studytonight.com/computer-networks/images/Figure32-1.png)

## PRESENTATION layer

- This is the sixth or second last layer of OSI model. This layer defines how the system provides files and services in
a uniform way to application. This layer can in some ways be considered the function of the operating system. When
data is transmitted between different types of computer systems, the presentation layer negotiates and manages the
way data is represented and encoded.


- ![pics](https://s3.ap-south-1.amazonaws.com/s3.studytonight.com/tutorials/uploads/pictures/1611059912-71449.png)

## APPLICATION LAYER

- The application layer is not an application. Instead, it is a component within an application that controls the communication method to other devices. It is an abstraction layer service that masks the rest of the application from the transmission process.

- The application layer relies on all the layers below it to complete its process. At this stage, the data or the application is presented in a visual form that the user can understand.

- ![pics](https://cf-assets.www.cloudflare.com/slt3lc6tev37/koKt5UKczRq47xJsexfBV/c1e1b2ab237063354915d16072157bac/7-application-layer.svg)

## The advantages of the OSI model

**1**.) It is a generic model and acts as a guidance tool to develop any network model.

**2**.) It is a layered model. Changes are one layer do not affect other layers, provided that the interfaces between the layers do not change drastically.

**3**.) It distinctly separates services, interfaces, and protocols. Hence, it is flexible in nature. Protocols in each layer can be replaced very conveniently depending upon the nature of the network.

 **4**.) It supports both connection-oriented services and connectionless services.

## The disadvantages of the OSI model 

**1**.)It is purely a theoretical model that does not consider the availability of appropriate technology. This restricts its practical implementation.

**2**.) The launching timing of this model was inappropriate. When OSI appeared, the TCP/IP protocols were already implemented. So, the companies were initially reluctant to use it.

**3**.) The OSI model is very complex. The initial implementation was cumbersome, slow and costly.

**4**.) Though there are many layers, some of the layers like the session layer and presentation layer have very little functionality when practically deployed.

**5**.) There is a duplication of services in various layers. Services like addressing, flow control and error control are offered by multiple layers.

**6**.) The standards of OSI model are theoretical and do not offer adequate solutions for practical network implementation.

**7**.) After being launched, the OSI model did not meet the practical needs as well as the TCP/IP model. So it was labeled as inferior quality.

**9**.) TCP/IP model was very much preferred by the academia. It was believed that OSI was a product of the European communities and the US government, who were trying to force an inferior model to researchers and programmers. Hence, there was considerable resistance in adopting it.

## CONCLUSION
In this paper we have tried to explain what exactly an OSI reference model is, why it is used and contribution of
various researchers in this reference. OSI is basically an architecture which only gives us an idea how packets
transfer over the network during any communication. OSI enhancements are done time to time for developing new
technologies. Proposed seven different layers in his paper for improvising security in any network. Future
implementation in OSI will lead to enhancement in security and many other fields.

## Reference
**1**.) https://www.tutorialspoint.com/The-OSI-Reference-Model.

**2**.)https://www.techtarget.com/searchnetworking/definition/OSI.

**3**.) https://www.tutorialspoint.com/The-OSI-Reference-Model.